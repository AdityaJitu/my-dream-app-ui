import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { routes } from './app.router';
import { LoginService } from './login/loginService';
import { UserDetailComponent } from './userDetail/userDetail.component';
import { TokenInterceptorService } from './interceptors';
import { UserDetailService } from './userDetail/userDetail.service';


@NgModule({
  declarations: [
    AppComponent, LoginComponent, UserDetailComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [LoginService, UserDetailService, {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
