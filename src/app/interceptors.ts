import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.indexOf('login') === -1) {
      const authValue = JSON.parse(localStorage.getItem('userInfo'))['token'];
      req = req.clone({
        setHeaders: {
          'x-access-token': authValue
        }
      });
    }
    return next.handle(req);
  }
}
