import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UserDetailComponent } from './userDetail/userDetail.component';

export const routes: Routes = [
 { path: 'login', component: LoginComponent },
{ path: 'userDetail/:userId', component: UserDetailComponent},
{ path: '', redirectTo: '/login', pathMatch: 'full' },
{ path: '**', component: LoginComponent}];
