import { IUserDetail, IContactDetail } from '../interface';
export class UserDetailModel implements IUserDetail {
    userId: string;
    fname: string;
    lname: string;
    country: string;
    contactDetails: any[] | IContactDetail[];
}
