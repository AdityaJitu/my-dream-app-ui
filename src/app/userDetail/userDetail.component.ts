import { Component, OnInit } from '@angular/core';
import { UserDetailModel } from './userDetailModel';
import { Title } from '@angular/platform-browser';
import { UserDetailService } from './userDetail.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
    selector: 'app-userdetail-component',
    templateUrl: './userDetailComponent.html'
})

export class UserDetailComponent implements OnInit {
    userDetail = new UserDetailModel();
    userDetailForm: FormGroup;
    toogleReadOnly = true;
    constructor(private title: Title,
        private userDetailService: UserDetailService,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder) {
            this.prepareFormData();
        }

    prepareFormData = () => {
        this.userDetailForm = this.fb.group({
            fname: [''],
            lname: [''],
            uname: [''],
            contactDetail: this.fb.array([])
        });
    }

    ngOnInit () {
        this.activatedRoute.paramMap
    .switchMap((params: ParamMap) =>
      this.userDetailService.
        fetchDetail(params.get('userId')))
            .subscribe( data => {
                console.log(data);
                // const bla = data.data['userDetail'];
                this.userDetailForm.patchValue(data['data']['userDetail']);
            });
        this.title.setTitle('User Detail');
    }
}
