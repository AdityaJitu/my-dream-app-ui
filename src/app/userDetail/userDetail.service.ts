import {Injectable} from '@angular/core';
import { IUserDetail, IUserDetailService } from '../interface';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserDetailService implements IUserDetailService {
    constructor(private http: HttpClient) {}
    userDetailUrl = environment.userDetail;
    fetchDetail = (userId: string): Observable<Object> => {
        // let data = {'userId': userId};
        return this.http.get(this.userDetailUrl, {params: {'userId': userId}});
    };
}