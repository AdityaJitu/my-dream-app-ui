import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { USERAUTH, ILoginService } from '../interface';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class LoginService implements ILoginService {
    constructor(private http: HttpClient) {}
    loginUrl = environment.loginUrl;
    logoutUrl = environment.loginUrl;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };
    logout: (authToken: string) => Observable<Object>;
    login = (userCred: USERAUTH): Observable<Object> => {
        return this.http.post(this.loginUrl, userCred, this.httpOptions);
    }
}
