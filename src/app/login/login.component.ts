   /* this.router.events.subscribe(s => {
                  if (s instanceof NavigationEnd) {
                    const tree = router.parseUrl(router.url);
                    if (tree.fragment) {
                      // you can use DomAdapter
                      const element = document.querySelector('#' + tree.fragment);
                      if (element) { element.scrollIntoView(element); }
                    }
                  }
                }); */
        /* gotoHashtag = (fragment: string) => {
            const element = document.querySelector('#' + fragment);
            if (element) {element.scrollIntoView(element); };
        } */

// Example of Template Driven Form

/* import { Component, AfterViewChecked } from '@angular/core';
import { LoginModel } from './loginModel';
import { USERAUTH } from '../interface';
import { LoginService } from './loginService';
import { Router, NavigationEnd } from '@angular/router';
@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html'
})

export class LoginComponent {
    title = 'Hello WOrld';
    userAuth = new LoginModel();
    constructor(private loginService: LoginService, private router: Router) {}
    signIn = () => {
        this.loginService.login(this.userAuth).subscribe(userCred => {
            console.log(userCred);
        }, err => {
            console.log(err);
        });
    }
} */

// Example of  Reactive Form
import { Component, OnInit } from '@angular/core';
import { LoginModel } from './loginModel';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { LoginService } from './loginService';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
    userAuth = new LoginModel();
    // username = new FormControl(); //use it only there is a single form control in a form
    userAuthForm: FormGroup;
    /* userAuthForm = new FormGroup({  //this is a good approach to create a group of form controls to easily manage stuff,
        username: new FormControl()
    }) */
    constructor(private fb: FormBuilder, private loginService: LoginService, private router: Router, private title: Title) {
        this.createForm();
    }

    createForm() {
        this.userAuthForm = this.fb.group({
            username: ['', Validators.required],
            password: ''
        });
    }

    signIn = () => {
        this.loginService.login(this.userAuthForm.value).subscribe( success => {
            // const { username } = success;
            localStorage.setItem('userInfo', JSON.stringify({ 'userid': success['id'],
                'username': success['username'], 'token': success['token'] }));
            this.router.navigate(['/userDetail', success['id']]);
        }, err => {
            console.log(err);
        });
    }

    ngOnInit(): void {
        this.title.setTitle('Login Form');
    }
}
