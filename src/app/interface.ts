import { Observable } from 'rxjs/Observable';

export interface USERAUTH {
    username: string;
    password: string;
}

export interface ILoginService {
    loginUrl: string;
    logoutUrl: string;
    login: (userCred: USERAUTH ) => Observable<Object>;
    logout: (authToken: string) => Observable<Object>;
}

export interface IContactDetail {
    email: string;
    contactno: string;
}
export interface IUserDetail {
    userId: string;
    fname: string;
    lname: string;
    country: string;
    contactDetails: Array<any> | Array<IContactDetail>;
}
export interface IUserDetailService {
    userDetailUrl: string;
    fetchDetail: (userId: string ) => Observable<Object>;
}
